//
//  RKTUser.m
//  RestKitTwitter
//
//  Created by Des Hartman on 5/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RKTUser.h"
#import "RKTStatus.h"


@implementation RKTUser

@dynamic screenName;
@dynamic userID;
@dynamic name;
@dynamic status;

@end
