//
//  RKTwitterObjectCache.m
//  RestKitTwitter
//
//  Created by Des Hartman on 5/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RKTwitterObjectCache.h"
#import "RKTStatus+Mapping.h"



#define RESTKIT_TWEETS @"/status/user_timeline/RestKit"

@implementation RKTwitterObjectCache




#pragma mark Fetch Requests

-(NSArray *)fetchRequestsForResourcePath:(NSString *)resourcePath {
	NSLog(@"*********************** fetchRequestsForResourcePath");
	
	if ([resourcePath isEqualToString:RESTKIT_TWEETS]) {
		
		NSFetchRequest* fetchRequest = [RKTStatus fetchRequest];
		NSSortDescriptor* descriptor = [NSSortDescriptor sortDescriptorWithKey:@"createdAt" ascending:YES];
		[fetchRequest setSortDescriptors:[NSArray arrayWithObject:descriptor]];
		
		
		//	NSLog(@"fetchReq.predicate %@", fetchRequest.predicate);
		//NSLog(@"fetchReq.sortDescriptors %@", fetchRequest.sortDescriptors);
		
		return [NSArray arrayWithObject:fetchRequest];
	}
	
	//else
	return Nil;
}

@end
