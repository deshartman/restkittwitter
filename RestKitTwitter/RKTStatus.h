//
//  RKTStatus.h
//  RestKitTwitter
//
//  Created by Des Hartman on 5/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RKTUser;

@interface RKTStatus : NSManagedObject

@property (nonatomic, retain) NSString * urlString;
@property (nonatomic, retain) NSNumber * statusID;
@property (nonatomic, retain) NSNumber * isFavorited;
@property (nonatomic, retain) NSString * inReplyToScreenName;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) RKTUser *user;

@end
