//
//  RKTUser+Mapping.h
//  RestKitTwitter
//
//  Created by Des Hartman on 30/01/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RKTUser.h"
#import <RestKit/RestKit.h>

@interface RKTUser (Mapping)

+(RKManagedObjectMapping *)userMapping;

@end
