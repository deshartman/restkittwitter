//
//  TwitterTableViewController.m
//  RestKitTwitter
//
//  Created by Des Hartman on 30/01/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "TwitterTableViewController.h"

#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>

#import "RestKitController.h"

#import "RKTUser+Mapping.h"
#import "RKTStatus+Mapping.h"


#define RESTKIT_TWEETS @"/status/user_timeline/RestKit"

@interface TwitterTableViewController () <RKObjectLoaderDelegate>

	@property (strong, nonatomic) NSManagedObjectContext *twitterTableContext;

@end


@implementation TwitterTableViewController

	@synthesize twitterTableContext = _twitterTableContext;


- (void)loadDataFromNetwork {
	
	NSLog(@"*********************** loadDataFromNetwork");

    RKObjectManager* objectManager = [RKObjectManager sharedManager];	
	
    [objectManager loadObjectsAtResourcePath:RESTKIT_TWEETS delegate:self block:^(RKObjectLoader* loader) {
        // Twitter returns statuses as a naked array in JSON, so we instruct the loader to use the appropriate object mapping
        loader.objectMapping = [objectManager.mappingProvider objectMappingForClass:[RKTStatus class]];
    }];
}

#pragma mark RKObjectLoaderDelegate methods

// Delegate methods for loadObjectsAtResourcePath

-(void)objectLoaderDidFinishLoading:(RKObjectLoader *)objectLoader {
	
	NSLog(@"*********************** objectLoaderDidFinishLoading");
	
	[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"LastUpdatedAt"];
	[[NSUserDefaults standardUserDefaults] synchronize];
	
	self.fetchedResultsController = [RKTStatus fetchAllSortedBy:@"createdAt" ascending:YES withPredicate:nil groupBy:nil];
	
}

- (void)objectLoader:(RKObjectLoader*)objectLoader didFailWithError:(NSError*)error {
	NSLog(@"Hit error: %@", error);
}


#pragma mark - IBActions

- (IBAction)refresh:(UIBarButtonItem *)sender {
	NSLog(@"*********************** refresh");
	[self loadDataFromNetwork];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
	NSLog(@"*********************** viewDidLoad");
    [super viewDidLoad];
	
	// Initialise shared instance of RestKit
	[RestKitController sharedRestKitController];
	
	self.fetchedResultsController = [RKTStatus fetchAllSortedBy:@"createdAt" ascending:YES withPredicate:nil groupBy:nil];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	
	NSDate* lastUpdatedAt = [[NSUserDefaults standardUserDefaults] objectForKey:@"LastUpdatedAt"];
	NSString* dateString = [NSDateFormatter localizedStringFromDate:lastUpdatedAt dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterMediumStyle];
	if (nil == dateString) {
		dateString = @"Never";
	}
	return [NSString stringWithFormat:@"Last Load: %@", dateString];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Tweet Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];		
    }
    
    // Configure the cell...
	RKTStatus* status = [self.fetchedResultsController objectAtIndexPath:indexPath];
	
	cell.textLabel.text = status.text;
	return cell;
}



@end
