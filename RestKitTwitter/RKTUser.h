//
//  RKTUser.h
//  RestKitTwitter
//
//  Created by Des Hartman on 5/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class RKTStatus;

@interface RKTUser : NSManagedObject

@property (nonatomic, retain) NSString * screenName;
@property (nonatomic, retain) NSNumber * userID;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *status;
@end

@interface RKTUser (CoreDataGeneratedAccessors)

- (void)addStatusObject:(RKTStatus *)value;
- (void)removeStatusObject:(RKTStatus *)value;
- (void)addStatus:(NSSet *)values;
- (void)removeStatus:(NSSet *)values;

@end
