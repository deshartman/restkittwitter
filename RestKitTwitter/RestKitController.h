//
//  RestKitController.h
//  PluggConsumer
//
//  Created by Des Hartman on 2/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>


#define PLUGG_SERVER_URL @"www.wiredlizard.com"	// Define
#define URL_JOBS @"/jobs"


@interface RestKitController : NSObject

@property (strong, nonatomic) RKManagedObjectStore *managedObjectStore;

+(RestKitController *)sharedRestKitController;

-(BOOL)isNetworkAvailable;	// Check the last know network status



@end
