//
//  RKTStatus.m
//  RestKitTwitter
//
//  Created by Des Hartman on 5/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RKTStatus.h"
#import "RKTUser.h"


@implementation RKTStatus

@dynamic urlString;
@dynamic statusID;
@dynamic isFavorited;
@dynamic inReplyToScreenName;
@dynamic text;
@dynamic createdAt;
@dynamic user;

@end
