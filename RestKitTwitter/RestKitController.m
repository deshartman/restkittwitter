//
//  RestKitController.m
//  PluggConsumer
//
//  Created by Des Hartman on 2/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RestKitController.h"

#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import <RestKit/RKManagedObjectCache.h>

#import "RKTwitterObjectCache.h"


#import "RKTUser+Mapping.h"
#import "RKTStatus+Mapping.h"

#define RESTKIT_TWEETS @"/status/user_timeline/RestKit"



static RestKitController *sharedRestKitController = nil;		// Used for Singleton creation


@interface RestKitController ()
	@property (strong, nonatomic) RKReachabilityObserver *reachabilityObserver;	// Global reachability
	@property (nonatomic) BOOL networkAvailable;
@end


@implementation RestKitController

@synthesize managedObjectStore = _managedObjectStore;
	@synthesize reachabilityObserver = _reachabilityObserver;
	@synthesize networkAvailable = _networkAvailable;


#pragma mark Network Reachability Checks
- (void)reachabilityChanged:(NSNotification *)notification {
	
	RKReachabilityObserver* observer = (RKReachabilityObserver *) [notification object];
	self.reachabilityObserver = observer;
    
    RKLogCritical(@"Received reachability update: %@", observer);
    NSLog(@"Host: %@ -> %@", observer.host, [observer reachabilityFlagsDescription]);
    
    if ([observer isNetworkReachable]) {
        if ([observer isConnectionRequired]) {
            NSLog(@"Connection is available...");
			self.networkAvailable = YES;
            return;
        }
        if (RKReachabilityReachableViaWiFi == [observer networkStatus]) {
            NSLog(@"Online via WiFi");
			self.networkAvailable = YES;
        } else if (RKReachabilityReachableViaWWAN == [observer networkStatus]) {
			NSLog(@"Online via 3G or Edge");
			self.networkAvailable = YES;
        }
    } else {
		NSLog(@"Network UNREACHABLE!");
		self.networkAvailable = NO;
    }
}

-(BOOL)isNetworkAvailable {

	return self.networkAvailable;
}

#pragma RestKit Initialisation

-(void)initRestKit {
	
	// Initialize object store
#ifdef RESTKIT_GENERATE_SEED_DB
	NSString *seedDatabaseName = nil;
	NSString *databaseName = RKDefaultSeedDatabaseFileName;
#else
	NSString *seedDatabaseName = RKDefaultSeedDatabaseFileName;
	NSString *databaseName = @"RKTwitterData.sqlite";
#endif
	
#ifdef RESTKIT_GENERATE_SEED_DB
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelInfo);
    RKLogConfigureByName("RestKit/CoreData", RKLogLevelTrace);
    RKManagedObjectSeeder* seeder = [RKManagedObjectSeeder objectSeederWithObjectManager:objectManager];
    
    // Seed the database with instances of RKTStatus from a snapshot of the RestKit Twitter timeline
    [seeder seedObjectsFromFile:@"restkit.json" withObjectMapping:statusMapping];
    
    // Seed the database with RKTUser objects. The class will be inferred via element registration
    [seeder seedObjectsFromFiles:@"users.json", nil];
    
    // Finalize the seeding operation and output a helpful informational message
    [seeder finalizeSeedingAndExit];
    
    // NOTE: If all of your mapped objects use keyPath -> objectMapping registration, you can perform seeding in one line of code:
    // [RKManagedObjectSeeder generateSeedDatabaseWithObjectManager:objectManager fromFiles:@"users.json", nil];
#endif
	
	
	
	// STEP 1 - Initialize RestKit
	// NOTE: This sets up a sharedObjectManager accessible via [RKObjectManager sharedManager];
	RKObjectManager* objectManager = [RKObjectManager objectManagerWithBaseURL:@"http://twitter.com"];
	
	/* STEP 2 - RKManagedObjectStore – 
	 The object store wraps the initialization and configuration of internal Core Data classes including 
	 NSManagedObjectModel, NSPersistentStoreCoordinator, and NSManagedObjectContext. The object store is also 
	 responsible for managing object contexts for each thread and managing changes between threads. In general, 
	 the object store seeks to remove as much boilerplate Core Data code as possible from the main application.
	 */
    objectManager.objectStore = [RKManagedObjectStore objectStoreWithStoreFilename:databaseName usingSeedDatabaseName:seedDatabaseName managedObjectModel:nil delegate:self];
	
	// Mappings for Objects are done in Object+Mapping category classes
	// Register our mappings with the provider
    [objectManager.mappingProvider setMapping:[RKTUser userMapping] forKeyPath:@"user"];
    [objectManager.mappingProvider setMapping:[RKTStatus statusMapping] forKeyPath:@"status"];
	
	// Step 3 - Create the ObjectCache and hook it up to the store
	//RKObjectCache* cache = [[RKObjectCache alloc] init];
	objectManager.objectStore.managedObjectCache = [[RKTwitterObjectCache alloc] init];
	
	// WORKAROUND - keep a strong pointer to the managedObjectContext
	
	self.managedObjectStore = objectManager.objectStore;
	NSLog(@"RestKitController:managedObjectStore: %@", self.managedObjectStore);
		
}

#pragma mark Singleton Methods
// Check for singleton instance of the database
+(RestKitController *)sharedRestKitController {
	
	@synchronized(self) {
	if (sharedRestKitController == nil)
		NSLog(@"Establishing a sharedRestKitController");
		sharedRestKitController = [[self alloc] init];
	}
	NSLog(@"return the sharedRestKitController singleton");
	return sharedRestKitController;
	
}

- (id)init {
	if (self = [super init]) {
		[self initRestKit];
	}
	return self;
}


@end
