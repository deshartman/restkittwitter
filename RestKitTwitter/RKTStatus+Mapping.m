//
//  RKTStatus+Mapping.m
//  RestKitTwitter
//
//  Created by Des Hartman on 30/01/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RKTStatus+Mapping.h"
#import "RKTUser+Mapping.h"

@implementation RKTStatus (Mapping)

/*
 Mapping by entity. Here we are configuring a mapping by targetting a Core Data entity with a specific name. 
 This allows us to map back Twitter user objects directly onto NSManagedObject instances.
 */
+(RKManagedObjectMapping *)statusMapping {
	
	RKManagedObjectMapping *statusMapping = [RKManagedObjectMapping mappingForClass:[self class]];
	
	[statusMapping mapKeyPathsToAttributes:@"id", @"statusID",
	 @"created_at", @"createdAt",
	 @"text", @"text",
	 @"url", @"urlString",
	 @"in_reply_to_screen_name", @"inReplyToScreenName",
	 @"favorited", @"isFavorited", 
	 nil];

	// Define the Primary key for the Object
	statusMapping.primaryKeyAttribute = @"statusID";
	
	// Define relationship of this object to others
	[statusMapping mapRelationship:@"user" withMapping:[RKTUser userMapping]];
	
	return statusMapping;
}



@end
