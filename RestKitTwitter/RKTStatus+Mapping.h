//
//  RKTStatus+Mapping.h
//  RestKitTwitter
//
//  Created by Des Hartman on 30/01/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RKTStatus.h"
#import <RestKit/RestKit.h>

@interface RKTStatus (Mapping)

+(RKManagedObjectMapping *)statusMapping;

@end
