//
//  RKTUser+Mapping.m
//  RestKitTwitter
//
//  Created by Des Hartman on 30/01/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import "RKTUser+Mapping.h"

#import "RKTStatus+Mapping.h"

@implementation RKTUser (Mapping)

/*
 Mapping by entity. Here we are configuring a mapping by targetting a Core Data entity with a specific name. 
 This allows us to map back Twitter user objects directly onto NSManagedObject instances.
 */
+(RKManagedObjectMapping *)userMapping {
	
	RKManagedObjectMapping *userMapping = [RKManagedObjectMapping mappingForClass:[self class]];
	
    [userMapping mapKeyPath:@"id" toAttribute:@"userID"];
    [userMapping mapKeyPath:@"screen_name" toAttribute:@"screenName"];
    [userMapping mapAttributes:@"name", nil];
	
	userMapping.primaryKeyAttribute = @"userID";
	
	return userMapping;	 
}

@end
