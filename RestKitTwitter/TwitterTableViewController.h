//
//  TwitterTableViewController.h
//  RestKitTwitter
//
//  Created by Des Hartman on 30/01/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"

@interface TwitterTableViewController : CoreDataTableViewController 


@end
