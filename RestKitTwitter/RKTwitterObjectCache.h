//
//  RKTwitterObjectCache.h
//  RestKitTwitter
//
//  Created by Des Hartman on 5/02/12.
//  Copyright (c) 2012 Des Hartman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import <RestKit/RKManagedObjectCache.h>

/**
 * An implementation of the RestKit object cache. The object cache is
 * used to return locally cached objects that live in a known resource path.
 * This can be used to avoid trips to the network.
 */
@interface RKTwitterObjectCache : NSObject <RKManagedObjectCache>

@end
